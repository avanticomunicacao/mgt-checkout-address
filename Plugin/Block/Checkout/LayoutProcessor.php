<?php
namespace Avanti\CheckoutAddress\Plugin\Block\Checkout;

use Magento\Checkout\Block\Checkout\LayoutProcessor as LayoutProcessorCore;

class LayoutProcessor
{
    public function afterProcess(
        LayoutProcessorCore $subject,
        array $jsLayout
    ) {
        $jsLayout = $this->getBillingFormFields($jsLayout);
        $jsLayout = $this->getShippingFormFields($jsLayout);
        return $jsLayout;
    }

    private function getShippingFormFields($jsLayout)
    {
        $jsLayout["components"]["checkout"]["children"]["steps"]["children"]
        ["shipping-step"]["children"]["shippingAddress"]["children"]["shipping-address-fieldset"]["children"]["street"]
        ["children"][0]["label"] = __("Street");

        $jsLayout['components']['checkout']['children']['steps']['children']
        ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['street']
        ['children'][1]['label'] = __('Number');
        $jsLayout['components']['checkout']['children']['steps']['children']
        ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['street']['children'][1]['validation']['required-entry'] = false;

        $jsLayout['components']['checkout']['children']['steps']['children']
        ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['street']
        ['children'][2]['label'] = __('Complement');
        $jsLayout['components']['checkout']['children']['steps']['children']
        ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['street']['children'][2]['validation']['required-entry'] = false;

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['street']
        ['children'][3]['label'] = __('Neighborhood');
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']
        ['street']['children'][3]['validation'] = ['required-entry' => true, "min_text_length" => 1, "max_text_length" => 255];

        return $jsLayout;
    }

    private function getBillingFormFields($jsLayout)
    {
        $jsLayout["components"]["checkout"]["children"]["steps"]["children"]["billing-step"]
        ["children"]["payment"]["children"]["afterMethods"]["children"]
        ["billing-address-form"]["children"]["form-fields"]["children"]["street"]["required"] = false;
        $jsLayout["components"]["checkout"]["children"]["steps"]["children"]["billing-step"]
        ["children"]["payment"]["children"]["afterMethods"]["children"]
        ["billing-address-form"]["children"]["form-fields"]["children"]["street"]["label"] = '';

        $jsLayout["components"]["checkout"]["children"]["steps"]["children"]["billing-step"]["children"]["payment"]
        ["children"]["afterMethods"]["children"]["billing-address-form"]["children"]
        ["form-fields"]["children"]["street"]["children"][0]['label'] = __("Street");

        $jsLayout["components"]["checkout"]["children"]["steps"]["children"]["billing-step"]["children"]["payment"]
        ["children"]["afterMethods"]["children"]["billing-address-form"]["children"]
        ["form-fields"]["children"]["street"]["children"][1]['label'] = __("Number");
        $jsLayout["components"]["checkout"]["children"]["steps"]
        ["children"]["billing-step"]["children"]["payment"]["children"]["afterMethods"]
        ["children"]["billing-address-form"]["children"]["form-fields"]["children"]["street"]
        ["children"][1]["validation"]["required-entry"] = true;

        $jsLayout["components"]["checkout"]["children"]["steps"]["children"]["billing-step"]["children"]["payment"]
        ["children"]["afterMethods"]["children"]["billing-address-form"]["children"]
        ["form-fields"]["children"]["street"]["children"][2]['label'] = __('Complement');

        $jsLayout["components"]["checkout"]["children"]["steps"]
        ["children"]["billing-step"]["children"]["payment"]["children"]["afterMethods"]
        ["children"]["billing-address-form"]["children"]["form-fields"]["children"]["street"]
        ["children"][2]["validation"]["required-entry"] = false;

        $jsLayout["components"]["checkout"]["children"]["steps"]["children"]["billing-step"]["children"]["payment"]
        ["children"]["afterMethods"]["children"]["billing-address-form"]["children"]
        ["form-fields"]["children"]["street"]["children"][3]['label'] = __('Neighborhood');

        $jsLayout["components"]["checkout"]["children"]["steps"]
        ["children"]["billing-step"]["children"]["payment"]["children"]["afterMethods"]
        ["children"]["billing-address-form"]["children"]["form-fields"]["children"]["street"]
        ["children"][3]["validation"]["required-entry"] = true;

        $jsLayout["components"]["checkout"]["children"]["steps"]["children"]
        ["billing-step"]["children"]["payment"]["children"]["afterMethods"]
        ["children"]["billing-address-form"]["children"]["form-fields"]["children"]
        ["postcode"]['component'] = 'SystemCode_BrazilCustomerAttributes/js/shipping-address/address-renderer/zip-code';

        $jsLayout["components"]["checkout"]["children"]["steps"]["children"]
        ["billing-step"]["children"]["payment"]["children"]["afterMethods"]
        ["children"]["billing-address-form"]["children"]["form-fields"]["children"]
        ["postcode"]['config']['elementTmpl'] = 'SystemCode_BrazilCustomerAttributes/shipping-address/address-renderer/zip-code';

        return $jsLayout;
    }
}

